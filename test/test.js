var Box2CoinCrowdsale = artifacts.require("Box2CoinCrowdsale.sol");
var Box2CoinToken = artifacts.require("Box2CoinToken.sol");

contract('Box2CoinCrowdsale', function(accounts){

  it('should deploy the token and store the address', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      const token = await instance.token.call();
      assert(token, 'Token address couldnt be stored');
      done();
    });
  })

  it('one ETH should buy 4300 B2C Tokens in preICO', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      const data = await instance.sendTransaction({
        from: accounts[7],
        value: web3.toWei(1, "ether")
      });

      const tokenAddress = await instance.token.call();
      const box2coinToken = Box2CoinToken.at(tokenAddress);
      const tokenAmount = await box2coinToken.balanceOf(accounts[7]);

      assert.equal(tokenAmount.toNumber(), 4300000000000000000000, 'The sender didn\'t receive the tokens as per PreICO rate');
      done();
    })
  })

  it('should set variable `totalWeiRaisedDuringPreICO` correctly', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      var amount = await instance.totalWeiRaisedDuringPreICO.call();
      assert.equal(amount.toNumber(), web3.toWei(1, "ether"), 'Total ETH raised in PreICO was not calculated correctly');
      done();
    });
  })

  it('should transfer the ETH to wallet immediately in Pre ICO', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      let balanceOfBeneficiary = await web3.eth.getBalance(accounts[9]);
      balanceOfBeneficiary = Number(balanceOfBeneficiary.toString(10));

      await instance.sendTransaction({
        from: accounts[1],
        value: web3.toWei(2, "ether")
      });

      let newBalanceOfBeneficiary = await web3.eth.getBalance(accounts[9]);
      newBalanceOfBeneficiary = Number(newBalanceOfBeneficiary.toString(10));
      assert.equal(newBalanceOfBeneficiary, balanceOfBeneficiary + 2000000000000000000, 'ETH couldn\'t be transferred to the beneficiary');
      done();
    })
  })

  it('Should mint for pools', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      await instance.mintForPools(accounts[2]);
      const tokenAddress = await instance.token.call();
      const box2coinToken = Box2CoinToken.at(tokenAddress);
      const tokenAmount = await box2coinToken.balanceOf(accounts[2]);
      assert.equal(tokenAmount.toNumber(), 500000000000000000000000000, 'Not all tokens was minted for pools');
      done();
    })
  });


  it('should transfer the raised ETH to RefundVault during ICO', function(done){
        Box2CoinCrowdsale.deployed().then(async function(instance) {
            var vaultAddress = await instance.vault.call();
            let balance = await web3.eth.getBalance(vaultAddress);
            
            assert.equal(balance.toNumber(), 30000000000000000, 'ETH couldn\'t be transferred to the vault');
            done();
       });
    });



  it('Vault balance should be added to our wallet once ICO is over', function(done){
    Box2CoinCrowdsale.deployed().then(async function(instance) {
      let balanceOfBeneficiary = await web3.eth.getBalance(accounts[9]);
      balanceOfBeneficiary = balanceOfBeneficiary.toNumber();

       var vaultAddress = await instance.vault.call();
       let vaultBalance = await web3.eth.getBalance(vaultAddress);

       await instance.finish();

       let newBalanceOfBeneficiary = await web3.eth.getBalance(accounts[9]);
       newBalanceOfBeneficiary = newBalanceOfBeneficiary.toNumber();

       assert.equal(newBalanceOfBeneficiary, balanceOfBeneficiary + vaultBalance.toNumber(), 'Vault balance '+vaultBalance.toNumber()+'couldn\'t be sent to the wallet');
       done();

    })
  })
});
