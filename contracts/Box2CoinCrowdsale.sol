pragma solidity ^0.4.18;

import "./Box2CoinToken.sol";
import "zeppelin-solidity/contracts/crowdsale/CappedCrowdsale.sol";
import "zeppelin-solidity/contracts/crowdsale/RefundableCrowdsale.sol";

contract Box2CoinCrowdsale is CappedCrowdsale, RefundableCrowdsale {

  enum CrowdsaleStage { PreICO, ICO }
  CrowdsaleStage public stage = CrowdsaleStage.PreICO;

  uint256 public tokensForBounty = 250000000000000000000000000;
  uint256 public tokensForLiquidityPool = 500000000000000000000000000;
  uint256 public totalTokensForSaleDuringPreICO = 62500000000000000000000000; // 20 out of 60 B2C will be sold during PreICO

  uint256 public totalWeiRaisedDuringPreICO;

  event EthTransferred(string text);
  event EthRefunded(string text);

  function Box2CoinCrowdsale(
    uint256 _startTime,
    uint256 _endTime,
    uint256 _rate,
    address _wallet,
    uint256 _goal,
    uint256 _cap
    )
    CappedCrowdsale(_cap)
    FinalizableCrowdsale()
    RefundableCrowdsale(_goal)
    Crowdsale(_startTime, _endTime, _rate, _wallet) public {
    require(_goal <= _cap);
  }

  function createTokenContract() internal returns (MintableToken) {
    return new Box2CoinToken(); //deploy token
  }

  function () external payable {
    uint256 tokensThatWillBeMintedAfterPurchase = msg.value.mul(rate);
    if(token.totalSupply() + tokensThatWillBeMintedAfterPurchase > totalTokensForSaleDuringPreICO) {
      msg.sender.transfer(msg.value);
      EthRefunded("PreICO Limit Hit");
      return;
    }

    buyTokens(msg.sender);

    totalWeiRaisedDuringPreICO = totalWeiRaisedDuringPreICO.add(msg.value);
  }

  function forwardFunds() internal {
    wallet.transfer(msg.value);
    EthTransferred("forwarding funds to wallet");
  }

  function transferOwnershipOfToken(address _newOwner) public onlyOwner {
    require(msg.sender == owner);
    token.transferOwnership(_newOwner);
  }

  function mintForPools(address _liquidityPoolFund) public onlyOwner {
    token.mint(_liquidityPoolFund, tokensForLiquidityPool);
  }

  function mintForBounty(address _bountyFund) public onlyOwner {
    token.mint(_bountyFund, tokensForBounty);
  }

  function finish() public onlyOwner {

      require(!isFinalized);
      uint256 alreadyMinted = token.totalSupply();
      require(alreadyMinted < totalTokensForSaleDuringPreICO);

      finalize();
  }

}
