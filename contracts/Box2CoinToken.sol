pragma solidity ^0.4.18;

import "zeppelin-solidity/contracts/token/MintableToken.sol";

contract Box2CoinToken is MintableToken {
  string public name = "Box2Coin Token";
  string public symbol = "B2C";
  uint8 public decimals = 18;
}
