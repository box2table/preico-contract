var Box2CoinCrowdsale = artifacts.require("./Box2CoinCrowdsale.sol");

module.exports = function(deployer) {
  const startTime = Math.round((new Date(Date.now() - 86400000).getTime())/1000); // Yesterday
  const endTime = Math.round((new Date().getTime() + (86400000 * 36))/1000); // Today + 20 days
  deployer.deploy(Box2CoinCrowdsale,
    startTime,
    endTime,
    4300,
    "0xd8B2E7c67Ba7Ca412a9AFc2D651BAFe96608A513", // Replace this wallet address with the last one (10th account) from Ganache UI. This will be treated as the beneficiary address.
    150000000000000000000, // 150 ETH
    500000000000000000000  // 500 ETH
  );

  console.log(startTime, endTime);
};
