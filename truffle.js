const Web3 = require("web3");
const web3 = new Web3();
const WalletProvider = require("truffle-wallet-provider");
const Wallet = require('ethereumjs-wallet');
var HDWalletProvider = require("truffle-hdwallet-provider");
var infura_apikey = "3lhRdEifOkJJhhqtaoLQ"; // Either use this key or get yours at https://infura.io/signup. It's free.
var mnemonic = "man apology always comfort pause capable evolve round number woman dove slender";


var mainNetPrivateKey = new Buffer("d94f0ab0361dc9c41c458a0a55269da5adc7dffba1200f314281f8d7d9dd397e", "hex")
var mainNetWallet = Wallet.fromPrivateKey(mainNetPrivateKey);
var mainNetProvider = new WalletProvider(mainNetWallet, "https://mainnet.infura.io/3lhRdEifOkJJhhqtaoLQ");


module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 7545,
      gas: 4600000,
      network_id: "5777"
    },
    ropsten:  {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/" + infura_apikey),
      network_id: 3,
      gas: 4500000
    },
    mainnet: {
      provider: mainNetProvider,
      gas: 4600000,
      gasPrice: web3.toWei("20", "gwei"),
      network_id: "1",
    }
  },
  solc: {
     optimizer: {
       enabled: true,
       runs: 200
     }
  }
};
